﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour
{
    GridFormation gridFormation;

    [SerializeField]
    private List<GameObject> units;
    float sizeOfUnit;

    public int TotalUnits { get { return units.Count; } }
    // Start is called before the first frame update
    void Start()
    {
        gridFormation = this.GetComponent<GridFormation>();
    }

    /// <summary>
    /// Moves units to positions and face the given direction
    /// </summary>
    /// <param name="positions"></param>
    /// <param name="direction"></param>
    public void MoveUnits(List<Transform> positions, Vector3 direction)
    {
        // Movement Behaviour for units
        for(int i = 0; i < positions.Count; i++)
        {
            // Acquire unit and Positions
            GameObject unit = units[i];
            Vector3 pos = positions[i].position;

            // Apply movement to unit
            unit.transform.position = pos;


            float angle = Vector3.SignedAngle(units[i].transform.forward, direction, Vector3.up);   // rotate towards formation direction
            units[i].transform.Rotate(new Vector3(0, angle, 0f));
        }
    }
}
