﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridFormation : MonoBehaviour
{
    // Setup
    public Camera mainCam;
    public LayerMask groundMask;
    public GameObject prefab;
    public GameObject guide;

    // Drag
    private bool dragging;
    int rightMButton = 1;

    // distBtxtUnit
    [SerializeField]
    float gap;
    [SerializeField]
    float sizeOfUnit;

    // Imaginary Line
    Vector3 I;
    Vector3 E;
    Vector3 C;

    float angle = 0;
    float radius = 0;

    private int maxUnits;

    List<Transform> displayUnits;
    UnitManager unitManager;

    bool depth;
    [SerializeField]
    bool pascal;

    private void Start()
    {
        depth = true;
        displayUnits = new List<Transform>();
            
        unitManager = GetComponent<UnitManager>();   
    }

    private void Update()
    {
        Dragging(I, C);
        if (dragging)
        {
            RotateGuide();
            C = ClickedCoord();
        // Acquire data of radius
            float angledAligned = guide.transform.rotation.eulerAngles.y - 90;
            float newRadius = Vector3.Magnitude(C - I);
            if (angle != angledAligned || radius != newRadius)  // Don't calculate new positions if stationary
            {
                angle = angledAligned;
                radius = newRadius;
            List<Vector3> positions;
                if (pascal)
                {
                    positions = GFprocedure.GeneralPositionsPascal(I, C, sizeOfUnit, gap, maxUnits, angle);
                } else
                {
                    positions = GFprocedure.GeneralPositions(I, C, sizeOfUnit, gap, maxUnits, angle);
                }
                    
                int totalUnitsPlaced = PlaceDisplayUnits(positions);
            }
        }
    }

    /** Dragging Mechanisms**/
    private void Dragging(Vector3 I, Vector3 E)
    {
    // Begin tracing radius
    // --------------------
        if (Input.GetMouseButtonDown(rightMButton))
        {
            dragging = true;
            this.I = ClickedCoord();
            // reset unit display
            DestroyUnitDisplay();
            maxUnits = unitManager.TotalUnits;
            FillDisplayUnits(maxUnits);
            radius = 1f;        // one unit always on click position
                
            guide.transform.localPosition = this.I;
        }

        // End of Radius
        // -------------
        if (Input.GetMouseButtonUp(rightMButton))
        {
            dragging = false;
            this.E = ClickedCoord();
            DeactivateDisplayUnits(0); 
            unitManager.MoveUnits(displayUnits, -guide.transform.right);   // Face the direction of the formation


        }
    }

    /// <summary>
    /// Gets the coordinate clicked by the mouse
    /// </summary>
    /// <returns>Coordinate</returns>
    private Vector3 ClickedCoord()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;   //Coordinates of clicked position.
        }
        return clickedCoord;
    }

    /// <summary>
    /// Fills the list of display units that are used to show
    /// the current positions calculated
    /// </summary>
    /// <param name="amount"></param>
    private void FillDisplayUnits(int amount)
    {
        
        for(int i = 0; i < amount; i++)
        {
            GameObject unit = Instantiate(prefab);
            unit.SetActive(false);
            displayUnits.Add(unit.transform);
        }
    }

    /// <summary>
    /// Destroys then clears the list of display units
    /// </summary>
    private void DestroyUnitDisplay()
    {
        foreach(Transform unit in displayUnits)
        {
            Destroy(unit.gameObject);
        }
        displayUnits.Clear();
    }

    /// <summary>
    /// Places the display units on the positions calculated in grid formation
    /// </summary>
    /// <param name="positions"></param>
    /// <returns></returns>
    private int PlaceDisplayUnits(List<Vector3> positions)
    {
        int unitIndex = 0;
        for (int i = 0; i < positions.Count; i++)   // use positions count for safety
        {
            Transform unit = displayUnits[i];
            unit.position = positions[i];
            unit.gameObject.SetActive(true);
            unit.gameObject.transform.eulerAngles = guide.transform.eulerAngles - new Vector3(0f, +90, 0f); // Face direction of formation
            unitIndex++;
        }
        if (!depth)
        {
            DeactivateDisplayUnits(unitIndex);
        }
        
        return unitIndex;
    }

    private void DeactivateDisplayUnits(int index)
    {
        for (int i = index; i < displayUnits.Count; i++)
        {
            displayUnits[i].gameObject.SetActive(false);
        }
    }

    // Helps calculate the angle
    private void RotateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {
            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
    }

}

