# Unit Grid Formation
Implementation of Grid Formation that is discussed in <a href=""> Grid Formation Tutorial</a>.

# Components

## Unit Manager
An empty object in the hierarchy that consists of the following components:
* UnitManager
* GridFormation

## Gap
Gap between each unit

## Pascal
Manner of position units in the last row. 

## Guide
Serves to align the rotational axis. 

## Unit size
The unit size is determined by the scale of the object. 
Both the Z and X values must be the same value. The scale value is what is given to the 
Unit Manager.

## Movement
Movement is based on the Z and X axis. Y Axis is used for rotation.

## Prefabs

### UnitManager
Unit Manager will contain the unit prefab that is to be moved during the game.

### Grid Manager
Grid Manager will contain the prefab that is to be displayed while dragging.

Both these prefabs must have the same X/Z scale value in order for the behaviour to work appropriately.

## GFequation and GFprocedure

Contain the equations and procedures that enable the grid formation to work. 